# Fork setup

## Installation

1. fill in your credentials in `.env`
2. `npm i` install dependencies

## Getting started

- `npm run start` - for Mainnet fork 
- `npm run start:polygon` - for Polygon fork 
- `npm run start:avalanche` - for Avalanche fork 

The console will spit out you the rpc url and chainId, and string to browser console for AAVE
The fork is deleted on sigint.

## Getting test amount 

To get the test amount, enter the wallet address after starting the fork
